#### 拓展网站
UniApp框架：https://uniapp.dcloud.net.cn/quickstart-hx.html
HBuilderX官网：https://www.dcloud.io/hbuilderx.html
文档：https://uniapp.dcloud.net.cn/
微信小程序：https://developers.weixin.qq.com/miniprogram/dev/component/


#### 介绍
UniApp框架 == Vue框架 -> dist html + css + js

UniApp 集成性框架
小程序、Web网页、H5网页、App

App 两种 
    纯原生态：Android IOS 两个不同语言开发出来的App 纯原生态的应用
        优点：拓展性和原生性比较出众
        缺点：需要大量人力和物力 开发周期要长

    混合类：uniapp react-native 
        优点：开发周期短、一套代码打包出多个终端
        缺点：局限性，性能跟原生态是没法比的

我们用一套代码，编译出不同版本的项目


注册一个小程序
https://mp.weixin.qq.com/

#### 创建项目
使用vue-cli方式创建
1.全局安装 vue-cli
安装命令：npm install -g @vue/cli
查看版本：vue --version
2.创建Uniapp正式版的命令vue2的：
vue create -p dcloudio/uni-preset-vue uniapp
选择默认模版


build 打包命令
dev 开发模式的命令

H5端
yarn dev:h5
npm run dev:h5

小程序端
yarn dev:mp-weixin
npm run dev:mp-weixin

App端
yarn dev:app-plus
npm run dev:app-plus

运行：
手机直接数据线USB插到电脑上 要让手机 开启 开发者模式 USB传输模式  文件传输
电脑上装模拟器：很卡



在我们的UniApp项目中，我们是不能够去写html的标签

WEB网页 == 微信小程序(纯源代码)
html  == wxml 小程序自己定义的标签
css == wxss 跟css是完全一样
js == js,wxs javascript + vue


UniApp(vue框架) -> build(打包) -> web,h5(html/css/js)
UniApp(vue框架) -> build(打包) -> 微信小程序源代码(wxml/wxss/wxs)
UniApp(vue框架) -> build(打包) -> app(app资源)



应用生命周期(只要在打开应用的第一次才会运行 只运行1次)
onLaunch
onShow
onHide
onError


Uniapp页面生命周期(Vue生命周期一样的)
onLoad -> 加载
onShow -> 页面显示
onReady -> 页面渲染完成
onHide -> 页面隐藏
onUnload -> 页面卸载

UniApp路由 === pages.json == vue-router
pages -> 页面路由 当中第一个元素就是默认首页

manifest.json == vite.config.js 脚手架的配置文件

#### 安装
1.安装sass
npm i sass -D
yarn add sass

2.安装sass-loader，注意需要版本10，否则可能会导致vue与sass的兼容问题而报错
npm i sass-loader@10 -D
yarn add sass-loader@10

3.安装UI框架
https://www.uviewui.com/
npm install uview-ui@2.0.36
yarn add uview-ui@2.0.36

uViewUI的配置
https://www.uviewui.com/components/npmSetting.html

1、在main.js上引入UviewUI 并 挂载
import uView from "uview-ui";
Vue.use(uView)

2、uni.scss 引入 UviewUI样式 
@import 'uview-ui/theme.scss';

3、引入uView基础样式 在App.vue中给style标签加入lang="scss"属性
<style lang="scss">
	/* uviewui 基础样式 */
	@import "uview-ui/index.scss";
</style>

4、配置easycom组件模式pages.json
	"easycom": {
		"^u-(.*)": "uview-ui/components/u-$1/u-$1.vue"
	},

5、Cli模式额外配置 
如果是vue-cli模式的项目，还需要在项目根目录的vue.config.js文件中进行如下配置：
// vue.config.js，如没有此文件则手动创建
module.exports = {
    transpileDependencies: ['uview-ui']
}

近期uview无法加载第三方的字体图标库解决方案
https://blog.csdn.net/lfeishumomol/article/details/131128450


#### 页面和路由
API	说明
uni.navigateTo	保留当前页面，跳转到应用内的某个页面，使用 uni.navigateBack 可以返回到原页面
uni.redirectTo	关闭当前页面，跳转到应用内的某个页面
uni.reLaunch	关闭所有页面，打开到应用内的某个页面
uni.switchTab	跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
uni.navigateBack	关闭当前页面，返回上一页面或多级页面


#### 功能逻辑
$u <u-> 属于 uViewUI框架的
uni  属于uniapp框架

注意：小程序当中：发出去的请求 必须是：https的请求  如果不是不允许上线
本地开发可以使用http接口 但需要配置：  微信开发者工具-设置-项目设置-不效验合法域名、web-view(业务域名)、TLS版本以及HTTPS证书

报错：cURL error 60: SSL certificate problem: unable to get local issuer certificate
解决：https://blog.csdn.net/zhangweixbl4/article/details/136941292

授权登录流程：
    1.调用 uni.login 接口 换取临时凭证code 15分钟
    2.封装请求文件src/services/request.js  
        2.1 发送接口请求->后端http://www.fast.com/uni  
        2.2 在main.js // 引入请求服务require("./services/request")(app)
    3.创建后端 
        3.1 新建模块：php think build --module uni
        3.2 创建控制器 php think make:controller uni/Business
    4.调用 auth.code2Session 接口，换取 用户唯一标识 OpenID 、 用户在微信开放平台账号下的唯一标识UnionID（若当前小程序已绑定到微信开放平台账号） 和 会话密钥 session_key。
        AppID(小程序ID)：wx7d5828a34e6bc571
        AppSecret(小程序密钥)：61f9d1c6b0b8c12763b341f920e1d084

1个数据库 -> business id: 40 mobile:1388888 openid:''

云课堂 Vue商城 微信小程序(用来表示用户唯一身份标记符 openid UnionID(企业用户))

捆绑的动作 
输入 手机号 和 密码 能找到这个人 就说明这个人 是在我们平台注册过
捆绑动作 update 更新语句  openid = 自己的openid

输入 手机号 和 密码 都找不到这个人 就说明他是新用户 
直接自动 注册一个新用户 insert openid


登录判断的功能

cookie  基于web浏览器 伴随着 http 请求 一并进行发送的
session 基于服务器端

Storage 本地存储(app,小程序) 将数据直接的存入到 app的应用内部
localstorage('key', value)

<scroll-view> uniapp 组件 自带了 下拉刷新 和 上拉加载的自定义的事件
uniapp 页面提供的下拉刷新和上拉加载


onPullDownRefresh 监听用户拉动到页面顶部 一般用于下拉刷新
onReachBottom  监听用户拉动到页面的底部 一般用户上拉加载

#### 小程序上线 
后端 -> 我们自己的服务器 小程序的代码 -> 微信公众平台 -> 人工审核 
    代码体积：2M 
    1、js代码 
    2、css代码 也可以放到自己的服务器 
    3、图片资源 放到自己的服务器 然后用外链引入到项目中

不分包：打开小程序的时候 一次性加载全部的界面进来 时间周期 长


分包概念：
    主包：(主页面) 首页、底部导航栏 5个界面 2M 时间周期 短
    我的：/pages/business/index


    子包：2M
        subPackage: {
            '/pages/business/index': {
                pages: [
                    {
                        path: '/pages/business/profile',
                        style: {
                            title: '个人资料'
                        }
                    },
                    {
                        path: '/pages/business/address',
                        style: {
                            title: '收获地址'
                        }
                    },
                ]
            }
        }
uniapp - 代码 - 小程序请求的接口 强制性 必须是 https的地址 1、一定要确保你项目中所请求的接口是线上地址 2、公众平台 -> 开发设置 -> 服务器域名 3、编译小程序的代码,并进行测试 yarn dev:mp-weixin 开发代码编译 yarn build:mp-weixin 运行打包命令 4、将“打包”的源代码进行上传 5、小程序的类目设置 6、提交人为审核。审核通过后，发布上线

后端接口 - 代码 -> 服务器上面 宝塔Linux 1、系统配置 执行到线上的数据库中 2、数据库表里面的字段 执行到线上的数据库中 3、改变成 https的站点
    
