import Vue from 'vue'
import App from './App'
import './uni.promisify.adaptor'

// 引入UviewUI 并 挂载
import uView from "uview-ui";
Vue.use(uView)

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
  ...App
})

// 引入请求服务
require("./services/request")(app)

app.$mount()